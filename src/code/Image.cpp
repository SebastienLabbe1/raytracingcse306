#include "Image.h"
#include "Vector.h"
#include "Util.h"
#include <fstream>
#include <iostream>
#include <cmath>

Image::Image(std::string filename) {
    loadPpm(filename);
}

Image::Image(int height, int width) : height(height), width(width) {
    buffer = new Vector[height * width];

}

void Image::saveAsPpm(std::string filename, int max_val) {
    std::ofstream file(filename);
    file << "P3 " << width << " " << height << " " << max_val << "\n";
    for(int i = 0; i < height; i ++) {
        for(int j = 0; j < width; j ++) {
            for(int k = 0; k < 3; k ++) {
                int x = max_val * pow(buffer[i * width + j][k], 1/2.2);
                file << std::min(std::max(x, 0), max_val) << " ";
            }
        }
        file << "\n";
    }
}

void Image::loadPpm(std::string filename) {
    std::ifstream file(filename);
    double x;
    std::string buf;
    double maxval;
    file >> buf >> width >> height >> maxval;
    buffer = new Vector[width * height];
    for(int i = 0; i < height; i ++)
    for(int j = 0; j < width; j ++)
    for(int k = 0; k < 3; k ++) {
        file >> x;
        buffer[i * width + j][k] = x / maxval;
    }

}

void Image::loadPpmRaw(std::string filename) {
    std::ifstream file(filename);
    char x;
    std::string buf;
    double maxval;
    file >> buf >> width >> height >> maxval;
    buffer = new Vector[width * height];
    for(int i = 0; i < height; i ++)
    for(int j = 0; j < width; j ++)
    for(int k = 0; k < 3; k ++) {
        file >> x;
        buffer[0 * width + j][k] = int(uint8_t(x));
        std::cout << int(uint8_t(x)) << std::endl;
    }

}

void Image::spread(int i, int j, double spreadRate, double spreadChance) {
    spreadChance *= 0.9;
    Vector p = getPixel(i, j);
    if(rand() / double(RAND_MAX) < spreadChance) {
        setPixel(i - 1, j, p, spreadRate);
        spread(i - 1, j, spreadRate, spreadChance);
    }
    if(rand() / double(RAND_MAX) < spreadChance) {
        setPixel(i + 1, j, p, spreadRate);
        spread(i + 1, j, spreadRate, spreadChance);
    }
    if(rand() / double(RAND_MAX) < spreadChance) {
        setPixel(i, j - 1, p, spreadRate);
        spread(i, j - 1, spreadRate, spreadChance);
    }
    if(rand() / double(RAND_MAX) < spreadChance) {
        setPixel(i, j + 1, p, spreadRate);
        spread(i, j + 1, spreadRate, spreadChance);
    }
}

Vector Image::getPixel(const Vector& p) const{
    int i = round(p[1] * (height - 1));
    int j = round(p[2] * (width - 1));
    int k = int(p[0]) * unit;
    return buffer[i * width + j + k];
}

Vector Image::getPixel(int i, int j) const{
    if(i < 0 || i >= height || j < 0 || j >= width) return Vector();
    return buffer[i * width + j];
}

void Image::setPixel(int i, int j, const Vector& p) {
    if(i < 0 || i >= height || j < 0 || j >= width) return;
    buffer[i * width + j] = p;
}

void Image::setPixel(int i, int j, const Vector& p, double percent) {
    if(i < 0 || i >= height || j < 0 || j >= width) return;
    buffer[i * width + j] *= 1 - percent;
    buffer[i * width + j] += p * percent;
}

Image* Image::randomSmooth(int height, int width, double changeRate) {
    Image* image = new Image(height, width);
    for(int i = 0; i < height; i ++)
    for(int j = 0; j < width; j ++) {
        Vector c = Vector::random(changeRate);
        if(i > 0) {
            if(j > 0) {
                c += image->buffer[(i - 1) * width + j];
                c += image->buffer[i * width + j - 1];
                c /= 2;
            } else c += image->buffer[(i - 1) * width + j];
        } else {
            if(j > 0) c += image->buffer[i * width + j - 1];
            else c = Vector::random();
        }
        c.compWiseMax(Vector(0, 0, 0));
        c.compWiseMin(Vector(1, 1, 1));
        image->buffer[i * width + j] = c;
    }
    return image;
}

Image* Image::randomSpread(int height, int width, double spreadRate, double spreadChance) {
    Image* image = new Image(height, width);
    for(int k = 0; k < height * width / 100; k ++) {
        int i = rand() % height;
        int j = rand() % width;
        image->setPixel(i, j, Vector::random());
        image->spread(i, j, spreadRate, spreadChance);
    }
    return image;
}
