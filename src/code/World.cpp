#include "World.h"
#include "Util.h"
#include "Intersection.h"
#include "Image.h"
#include <iostream>
#include <math.h>

#define RAND (rand() / double(RAND_MAX))
#define RANDE (uniform(engine))
#define EPS 10e-6

World::World() {}

void World::insert(AABB* aabb) {
    aabb->computeAABB();
    root->insert(aabb);
}

void World::addLights(std::vector<Light> lis) {
    lights.insert(lights.end(), lis.begin(), lis.end());
}

void World::addSLights(std::vector<SLight> lis) {
    slights.insert(slights.end(), lis.begin(), lis.end());
}

void World::fillImage(Image* const image) const {
    if(!camera) throw std::runtime_error("FillImage called with null camera");
    if(!image) throw std::runtime_error("FillImage called with null image ");
    const double di = 1. / image->height;
    const double dj = 1. / image->width;
    const double r = 1. - 1. / camera->raysPerPixel;
    const double ri = di * r;
    const double rj = dj * r;
    #pragma omp parallel for schedule(dynamic, 1)
    for(int i = 0; i < image->height; i ++)
    for(int j = 0; j < image->width; j ++) {
        const double li = i * di;
        const double lj = j * dj;
        Vector pixel;
        for (int ir = 0; ir < camera->raysPerPixel; ir ++) {
            Ray ray = camera->getRay(li + ri * RANDE, lj + rj * RANDE);
            pixel += getColor(ray, air.refractionIndex, 
            camera->maxBounces, false);
        }
        image->setPixel(i, j, pixel / (camera->raysPerPixel * camera->startingIntensity));
    }
}      

Vector World::getColor(const Ray& ray, double refractionIndex, int bounces, bool isDiffused) const {
    if(!bounces) return Vector();
    //Get the next object in line of sight
    Intersection intersection;
    if (!root->intersect(ray, intersection)) return Vector();
    if(!intersection.object) throw std::runtime_error("intersect returns true with no object");
    if(intersection.object->properties.isLight) return (isDiffused) ? Vector() 
    : (intersection.object->properties.color
    * std::max( - dot(ray.direction, intersection.normal), 0.));

    //Check if the ray collides from inside or outside
    double proj = dot(intersection.normal, ray.direction);
    double nextRefractionIndex = intersection.object->properties.refractionIndex;
    if (proj > 0) {
        nextRefractionIndex = air.refractionIndex;
        intersection.normal *= -1;
        proj *= -1;
    }

    Vector collision = ray.origin + ray.direction * intersection.t;
    Vector color;
    double albedo = intersection.object->properties.albedo;

    if(!bounces || (RAND < albedo && intersection.color.norm2() > EPS)) {

        //Direct lighting
        Vector totalLight;
        if(!bounces || RAND < directProportion) {
            int j = rand() % slights.size();
            SLight light = slights[j];
            double lRadius = light.radius;
            Vector lCenter = light.center;
            Vector lColor = light.properties.color;
            Vector Nlight = (collision - lCenter).normalized();
            Vector N = Nlight.randomCos();
            Vector x = N * lRadius + lCenter;
            Vector lightDirection = x - collision;
            double lightDistance = lightDirection.norm();
            if(absd(lightDistance) < EPS) throw std::runtime_error("lightdistance 0");
            lightDirection /= lightDistance;
            Ray lightRay = Ray(collision, lightDirection);

            Intersection lightIntersection(lightDistance - EPS);
            double pdf = dotPos(N, Nlight);
            if(!root->intersect(lightRay, lightIntersection)) {
            if(pdf > camera->minIntensity) {
            if(absd(pdf) < EPS) throw std::runtime_error("pdf 0");
            double proj1 = dotPos(intersection.normal, lightDirection);
            double proj2 = dotPos(Vector() - N, lightDirection);
            double lightIntensity = albedo * proj1 * proj2 
            / (lightDistance * lightDistance * pdf); // R*R and 1 / pi omited
            totalLight += lColor * lightIntensity;
            }}
        } else {
            //Indirect lighting
            double diffusedRatio = rho_d / (rho_d + rho_s);
            double frac = (alpha + 8) / (8 * M_PI);
            if(RANDE < diffusedRatio) {
                Ray diffusedRay = Ray(collision, intersection.normal.randomCos());
                totalLight += getColor(diffusedRay, refractionIndex, bounces - 1, true) 
                * albedo / diffusedRatio;
            } else {
                double z; 
                Vector H = ray.direction.reflect(intersection.normal, proj).randomPow(alpha, z);
                Ray phongRay = Ray(collision, ray.direction.reflect(H));
                if(dot(phongRay.direction, intersection.normal) > 0) {
                double coef = rho_s * frac * pow(dotPos(intersection.normal, 
                (phongRay.direction - ray.direction).normalized()), alpha);
                double pdf_pow = pow(z, alpha) * (alpha + 1) / dotPos(ray.direction, H);
                if(pdf_pow > camera->minIntensity) {
                totalLight += getColor(phongRay, refractionIndex, bounces - 1, false) 
                * coef * dotPos(intersection.normal, phongRay.direction)
                / ((1 - diffusedRatio) * pdf_pow);
                }}
            }
        }
        color = intersection.color.compWiseMult(totalLight);
    } else {
        double R = reflectedProportion(refractionIndex, nextRefractionIndex, absd(proj));
        if(RAND < R) {
            Ray reflectedRay = Ray(collision, ray.direction.reflect(intersection.normal, proj));
            color += getColor(reflectedRay, refractionIndex, bounces - 1, false);
        } else {
            Ray refractedRay = Ray(collision, ray.direction.refract(intersection.normal,
            proj, refractionIndex, nextRefractionIndex));
            color += getColor(refractedRay, nextRefractionIndex, bounces - 1, false);
        }
    }
    return color;
}

Vector World::getColor(const Ray& ray, double refractionIndex, int bounces, int rayCount, 
bool isDiffused) const {
    //Get the next object in line of sight
    Intersection intersection;
    if (!root->intersect(ray, intersection)) return Vector();
    if(!intersection.object) throw std::runtime_error("intersect returns true with no object");
    if(intersection.object->properties.isLight) return (isDiffused) ? Vector() 
    : (intersection.object->properties.color * rayCount 
    * std::max( - dot(ray.direction, intersection.normal), 0.));

    //Check if the ray collides from inside or outside
    double proj = dot(intersection.normal, ray.direction);
    double nextRefractionIndex = intersection.object->properties.refractionIndex;
    if (proj > 0) {
        nextRefractionIndex = air.refractionIndex;
        intersection.normal *= -1;
        proj *= -1;
    }

    Vector collision = ray.origin + ray.direction * intersection.t;
    Vector color;
    double albedo = intersection.object->properties.albedo;

    int diffractedRays = (bounces) ? binomial(rayCount, albedo) : rayCount;

    if(diffractedRays > 0 && intersection.color.norm2()) {
        int diffusedRays = (bounces) ? binomial(diffractedRays, directProportion) : diffractedRays;

        //Direct lighting
        Vector totalLight;
        for(int i = 0; i < diffusedRays; i ++) {
            int j = rand() % slights.size();
            SLight light = slights[j];
            double lRadius = light.radius;
            Vector lCenter = light.center;
            Vector lColor = light.properties.color;
            Vector Nlight = (collision - lCenter).normalized();
            Vector N = Nlight.randomCos();
            Vector x = N * lRadius + lCenter;
            Vector lightDirection = x - collision;
            double lightDistance = lightDirection.norm();
            if(absd(lightDistance) < EPS) throw std::runtime_error("lightdistance 0");
            lightDirection /= lightDistance;
            Ray lightRay = Ray(collision, lightDirection);

            Intersection lightIntersection(lightDistance - EPS);
            if(root->intersect(lightRay, lightIntersection)) continue;

            double pdf = dotPos(N, Nlight);
            if(pdf < camera->minIntensity) continue;
            if(absd(pdf) < EPS) throw std::runtime_error("pdf 0");
            double proj1 = dotPos(intersection.normal, lightDirection);
            double proj2 = dotPos(Vector() - N, lightDirection);
            double lightIntensity = albedo * proj1 * proj2 
            / (lightDistance * lightDistance * pdf); // R*R and 1 / pi omited
            totalLight += lColor * lightIntensity;
        }

        //Indirect lighting
        int phongRays = diffractedRays - diffusedRays;
        double diffusedRatio = rho_d / (rho_d + rho_s);
        double frac = (alpha + 8) / (8 * M_PI);
        for(int i = 0; i < phongRays; i ++) {
            if(RANDE < diffusedRatio) {
                Ray diffusedRay = Ray(collision, intersection.normal.randomCos());
                totalLight += getColor(diffusedRay, refractionIndex, bounces - 1, 1, true) 
                * albedo / diffusedRatio;
            } else {
                double z; 
                Vector H = ray.direction.reflect(intersection.normal, proj).randomPow(alpha, z);
                Ray phongRay = Ray(collision, ray.direction.reflect(H));
                if(dot(phongRay.direction, intersection.normal) < 0) continue;
                double coef = rho_s * frac * pow(dotPos(intersection.normal, 
                (phongRay.direction - ray.direction).normalized()), alpha);
                double pdf_pow = pow(z, alpha) * (alpha + 1) / dotPos(ray.direction, H);
                if(pdf_pow < camera->minIntensity) continue;
                totalLight += getColor(phongRay, refractionIndex, bounces - 1, 1, false) 
                * coef * dotPos(intersection.normal, phongRay.direction)
                / ((1 - diffusedRatio) * pdf_pow);
            }
        }
        color = intersection.color.compWiseMult(totalLight);
    } 
    int bouncedRays = rayCount - diffractedRays;
    if(bouncedRays > 0) {
        double R = reflectedProportion(refractionIndex, nextRefractionIndex, absd(proj));
        int reflectedRays = binomial(bouncedRays, R);
        if(reflectedRays > 0) {
            Ray reflectedRay = Ray(collision, ray.direction.reflect(intersection.normal, proj));
            color += getColor(reflectedRay, refractionIndex, bounces - 1, reflectedRays, false);
        }
        int refractedRays = bouncedRays - reflectedRays;
        if(refractedRays > 0) {
            Ray refractedRay = Ray(collision, ray.direction.refract(intersection.normal,
            proj, refractionIndex, nextRefractionIndex));
            color += getColor(refractedRay, nextRefractionIndex, bounces - 1, refractedRays, false);
        }
    }
    return color;
}