#include "Object.h"
#include <iostream>

Object::Object(const MaterialProperties& properties, Image* texture = nullptr) 
: properties(properties), texture(texture){
}

AABB* Object::insert(AABB* aabb) {
    return new AABB(this, aabb);
}