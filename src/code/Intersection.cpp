#include "Intersection.h"

Intersection::Intersection(double t) : t(t) {
}

void Intersection::update(const Vector& normal, const Vector& color,
double t, const Object* object) {
    this->normal = normal;
    this->color = color;
    this->t = t;
    this->object = object;
}