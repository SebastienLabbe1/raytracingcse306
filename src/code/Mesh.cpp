#include "Mesh.h"
#include "Util.h"
#include "Image.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#define EPS 10e-6

#define CHECK_EDGES(i1, i2, collision, x, y, z, denom) \
double u, v; \
double t1 = collision[i1] - x[i1]; \
double t2 = collision[i2] - x[i2]; \
bool c1 = (((y[i1] - x[i1]) * t2) - ((y[i2] - x[i2]) * t1) > 0); \
if((( v = ((z[i1] - x[i1]) * t2) - (z[i2] - x[i2]) * t1) > 0) == c1) \
    return false; \
if((( u = (((z[i2] - y[i2]) * (collision[i1] - y[i1])) \
    - (z[i1] - y[i1]) * (collision[i2] - y[i2]))) > 0) == c1) \
    return false; \
if(c1) denom *= -1; \
u /= denom; \
v /= denom; \
\
intersection.update(barycenterValue(normals[triangles[iT].norm[0]], \
normals[triangles[iT].norm[1]], normals[triangles[iT].norm[2]], u, v), \
texture->getPixel(barycenterValue(uvs[triangles[iT].uv[0]], uvs[triangles[iT].uv[1]], \
uvs[triangles[iT].uv[2]], u, v)), t, this); \
\
return true;

Mesh::Mesh(std::vector<Vector> vertices, std::vector<Vector> normals, 
std::vector<Vector> uvs, std::vector<Triangle> triangles, 
const MaterialProperties& properties, Image* texture)
: Object(properties, texture), vertices(vertices), 
normals(normals), uvs(uvs), triangles(triangles) {
}

void Mesh::computeAABB() {
    vmin = vertices[triangles[0].vtx[0]];
    vmax = vmin;
    for(Triangle& triangle : triangles) {
        for(int iV : triangle.vtx) {
            vmin.compWiseMin(vertices[iV]);
            vmax.compWiseMax(vertices[iV]);
        }
    }
}

bool Mesh::intersectTriangle(const Ray& ray, Intersection& intersection, int iT) const{
    Vector x = vertices[triangles[iT].vtx[0]];
    Vector y = vertices[triangles[iT].vtx[1]];
    Vector z = vertices[triangles[iT].vtx[2]];

    Vector N = cross(y - x, z - x);
    double denom = N.norm();
    //double denom = dot(N, N);
    //TODO remove the sqrt and use only normsquared
 
    double proj = dot(N, ray.direction);
    if(absd(proj) < EPS) return false;

    double t = dot(N, x - ray.origin) / proj;
    if(t < EPS || t > intersection.t) return false;

    Vector collision = ray.origin + ray.direction * t;

    if(absd(N[0]) > absd(N[1])) {
        if(absd(N[0]) > absd(N[2])) {
            CHECK_EDGES(1, 2, collision, x, y, z, denom)
        }
        CHECK_EDGES(1, 0, collision, x, y, z, denom)
    }
    if(absd(N[1]) > absd(N[2])) {
        CHECK_EDGES(0, 2, collision, x, y, z, denom)
    }
    CHECK_EDGES(1, 0, collision, x, y, z, denom)
}

bool Mesh::intersectTriangle2(const Ray& ray, Intersection& intersection, int iT) const{
    //https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/barycentric-coordinates

    Vector x = vertices[triangles[iT].vtx[0]];
    Vector y = vertices[triangles[iT].vtx[1]];
    Vector z = vertices[triangles[iT].vtx[2]];

    Vector N = cross(y - x, z - x);
    double denom = dot(N, N);
 
    double proj = dot(N, ray.direction);
    if(absd(proj) < EPS) return false;

    double t = dot(N, x - ray.origin)/ proj;
    if(t < EPS || t > intersection.t) return false;
 
    Vector collision = ray.origin + ray.direction * t;
    double u, v;

    bool c1 = dot(N, cross(y - x, collision - x)) < 0;
    if (c1 != ((u = dot(N, cross(z - y, collision - y))) < 0)) return false;
    if (c1 != ((v = dot(N, cross(x - z, collision - z))) < 0)) return false;

    if(c1) denom *= -1;
    u /= denom; 
    v /= denom; 

    intersection.update(barycenterValue(normals[triangles[iT].norm[0]], 
    normals[triangles[iT].norm[1]], normals[triangles[iT].norm[2]], u, v),
    (texture) ? texture->getPixel(barycenterValue(uvs[triangles[iT].uv[0]], uvs[triangles[iT].uv[1]], 
    uvs[triangles[iT].uv[2]], u, v)) : properties.color, t, this);

    return true;
}

bool Mesh::intersect(const Ray& ray, Intersection& intersection) const{
    if(!intersectAABB(ray, intersection)) return false;
    bool inter = false;
    for (int i = 0; i < int(triangles.size()); i ++) 
        inter |= intersectTriangle(ray, intersection, i);
    return inter;
}

void Mesh::move(const Vector& translation, double size) {
    for(Vector& vert : vertices) 
        vert = vert * size + translation;
    computeAABB();
}

Mesh* Mesh::loadStl(std::string filename,
const MaterialProperties& properties, Image* image = nullptr) {
	std::ifstream file(filename);

    std::vector<Vector> vertices, normals, uvs;
	std::vector<Triangle> triangles;

	std::string buf;
	while(buf != "facet") file >> buf;
	while(buf == "facet") {
	    Vector n, v[3], uv[3];
		file >> buf >> n[0] >> n[1] >> n[2];
		file >> buf >> buf;
        for(int i = 0; i < 3; i ++) file >> buf >> v[i][0] >> v[i][1] >> v[i][2];
        for(int i = 0; i < 3; i ++) file >> buf >> uv[i][0] >> uv[i][1] >> uv[i][2];
		file >> buf >> buf >> buf;
        int vs = vertices.size();
        int ns = normals.size();
        int uvss = uvs.size();
        Triangle triangle({vs, vs + 1, vs + 2}, {ns, ns, ns}, {uvss, uvss + 1, uvss + 2});
        for(int i = 0; i < 3; i ++) vertices.push_back(v[i]);
        for(int i = 0; i < 3; i ++) uvs.push_back(uv[i]);
        normals.push_back(n);
        triangles.push_back(triangle);
	}

	Mesh* mesh = new Mesh(vertices, normals, uvs, triangles, properties, image);
	return mesh;
}