#include "Cube.h"
#include "Util.h"
#include "Intersection.h"
#include <cmath>
#include <iostream>
#define EPS 10e-5

Cube::Cube(Vector center, Vector diagonal, const MaterialProperties& properties,
Image* texture) : Object(properties, texture), center(center), diagonal(diagonal) {
}

bool Cube::intersect(const Ray& ray, Intersection& intersection) const {
    //std::cout << "intersect cube called" << std::endl;
    float t1 = (vmin[0] - ray.origin[0])*ray.frac[0];
    float t2 = (vmax[0] - ray.origin[0])*ray.frac[0];
    float t3 = (vmin[1] - ray.origin[1])*ray.frac[1];
    float t4 = (vmax[1] - ray.origin[1])*ray.frac[1];
    float t5 = (vmin[2] - ray.origin[2])*ray.frac[2];
    float t6 = (vmax[2] - ray.origin[2])*ray.frac[2];

    float tmax = std::min(std::min(std::max(t1, t2), std::max(t3, t4)), std::max(t5, t6));
    if(tmax < EPS) return false;
    float tmin = std::max(std::max(std::min(t1, t2), std::min(t3, t4)), std::min(t5, t6));
    if(tmax - tmin < EPS) return false;
    double t = (tmin < EPS) ? tmax : tmin;
    if(t > intersection.t) return false;
    Vector collision = ray.origin + ray.direction * t;
    Vector normal;
    Vector uv;
    bool found = false;
    for(int i = 0; i < 3; i ++) {
        if(!found) {
            if(absd(collision[i] - vmax[i]) < EPS) {found = true; normal[i] = 1;}
            else if(absd(collision[i] - vmin[i]) < EPS) {found = true; normal[i] = -1;}
            else uv[i + 1] = (vmax[i] - collision[i]) / (vmax[i] - vmin[i]);
        } else uv[i] = (vmax[i] - collision[i]) / (vmax[i] - vmin[i]);
    }
    intersection.update(normal, (texture) ? texture->getPixel(uv) : properties.color, t, this);
    return true;
}

void Cube::computeAABB() {
    vmin = center - diagonal;
    vmax = center + diagonal;
}