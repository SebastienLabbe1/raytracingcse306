#include "Ray.h"
#include "Util.h"
#include <cfloat>
#include <cmath>
#define offset 0.0001

Ray::Ray(Vector origin, Vector direction) : origin(origin), direction(direction) {
    frac = Vector((absd(direction[0]) < 0.00001) ? DBL_MAX : 1/direction[0], 
    (absd(direction[1]) < 0.00001) ? DBL_MAX : 1/direction[1], 
    (absd(direction[2]) < 0.00001) ? DBL_MAX : 1/direction[2]);
}