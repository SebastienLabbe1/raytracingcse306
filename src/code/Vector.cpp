#include "Vector.h"
#include "Util.h"
#include <cmath>
#define RAND (rand() / double(RAND_MAX))
#define RANDE (uniform(engine))



Vector::Vector(double x, double y, double z) {
	values[0] = x;
	values[1] = y;
	values[2] = z;
}

const double& Vector::operator[] (int i) const {
	return values[i];
}

double& Vector::operator[] (int i) {
	return values[i];
}

Vector& Vector::operator+= (const Vector& vec) {
	values[0] += vec[0];
	values[1] += vec[1];
	values[2] += vec[2];
	return *this;
}

Vector& Vector::operator-= (const Vector& vec) {
	values[0] -= vec[0];
	values[1] -= vec[1];
	values[2] -= vec[2];
	return *this;
}

Vector& Vector::operator*= (double r) {
	values[0] *= r;
	values[1] *= r;
	values[2] *= r;
	return *this;
}

Vector& Vector::operator/= (double r) {
	values[0] /= r;
	values[1] /= r;
	values[2] /= r;
	return *this;
}

double Vector::norm () const {
	return std::sqrt(values[0] * values[0] 
	+ values[1] * values[1] 
	+ values[2] * values[2]);
}

double Vector::norm2 () const {
	return values[0] * values[0] 
	+ values[1] * values[1] 
	+ values[2] * values[2];
}

void Vector::normalize() {
	*this /= norm();
}

Vector Vector::normalized() const {
	return *this / norm();
}

Vector Vector::getOrthogonal() const {
	if(absd(values[0]) < absd(values[1])) {
		if(absd(values[2]) < absd(values[0])) 
			return Vector( - values[1], values[0], 0).normalized();
		return Vector(0, - values[2], values[1]).normalized();
	}
	if(absd(values[2]) < absd(values[1])) 
		return Vector( - values[1], values[0], 0).normalized();
	return Vector(- values[2], 0, values[0]).normalized();
}

void Vector::copy(const Vector& vec) {
	values[0] = vec[0];
	values[1] = vec[1];
	values[2] = vec[2];
}

void Vector::clear() {
	values[0] = 0;
	values[1] = 0;
	values[2] = 0;
}

Vector& Vector::compWiseMin(const Vector& vec) {
	values[0] = std::min(vec[0], values[0]);
	values[1] = std::min(vec[1], values[1]);
	values[2] = std::min(vec[2], values[2]);
	return *this;
}

Vector& Vector::compWiseMax(const Vector& vec) {
	values[0] = std::max(vec[0], values[0]);
	values[1] = std::max(vec[1], values[1]);
	values[2] = std::max(vec[2], values[2]);
	return *this;
}

Vector& Vector::compWiseMult(const Vector& vec) {
	values[0] *= vec[0];
	values[1] *= vec[1];
	values[2] *= vec[2];
	return *this;
}

double Vector::min() const {
	return values[minI()];
}

double Vector::max() const {
	return values[maxI()];
}

int Vector::minI() const {
	if(values[0] < values[1]) {
		if(values[2] < values[0]) return 2;
		return 0;
	}
	if(values[2] < values[1]) return 2;
	return 1;
}

int Vector::maxI() const {
	if(values[0] > values[1]) {
		if(values[2] > values[0]) return 2;
		return 0;
	}
	if(values[2] > values[1]) return 2;
	return 1;
}

Vector Vector::randomCos() const {
	Vector tang1 = getOrthogonal();
	Vector tang2 = cross(*this, tang1);
	double r1 = RANDE;
	double r2 = RANDE;
	double phi = 2 * M_PI * r1;
	double root = sqrt(1 - r2);
	double x = cos(phi) * root;
	double y = sin(phi) * root;
	double z = sqrt(r2);
	return tang1 * x + tang2 * y + *this * z;
}

Vector Vector::randomPow(double alpha, double& z) const {
	Vector tang1 = getOrthogonal();
	Vector tang2 = cross(*this, tang1);
	double r1 = RANDE;
	double r2 = RANDE;
	double phi = 2 * M_PI * r1;
	z = pow(r2, 1./(alpha + 1));
	double root = sqrt(1 - z * z);
	double x = cos(phi) * root;
	double y = sin(phi) * root;
	Vector H = tang1 * x + tang2 * y + *this * z;
	return H;
}


Vector Vector::reflect(const Vector& normal) const {
	return *this - normal * (dot(*this, normal) * 2);
}

Vector Vector::refract(const Vector& normal, double n1, double n2) const {
	double proj = dot(*this, normal);
    double frac = n1 / n2;
    return *this * frac - normal * (frac * proj + 
	sqrt(1 - frac * frac * (1 - proj * proj)));
}

Vector Vector::reflect(const Vector& normal, double proj) const {
	return *this - normal * (proj * 2);
}

Vector Vector::refract(const Vector& normal, double proj, double n1, double n2) const {
    double frac = n1 / n2;
    return *this * frac - normal * (frac * proj + sqrt(1 - frac * frac * (1 - proj * proj)));
}

Vector Vector::random() {
	return Vector(RANDE, RANDE, RANDE);
}

Vector Vector::random(double r) {
	Vector x = Vector(0.5, 0.5, 0.5) - random();
	return x * (2 * r) ;
}

Vector operator+ (const Vector& a, const Vector& b) {
	return Vector(a[0] + b[0], a[1] + b[1], a[2] + b[2]);
}

Vector operator- (const Vector& a, const Vector& b) {
	return Vector(a[0] - b[0], a[1] - b[1], a[2] - b[2]);
}

Vector operator* (const Vector& a, double r) {
	return Vector(a[0] * r, a[1] * r, a[2] * r);
}

Vector operator/ (const Vector& a, double r) {
	return Vector(a[0] / r, a[1] / r, a[2] / r);
}

Vector compWiseMin(const Vector& a, const Vector& b) {
	return Vector(std::min(a[0], b[0]), std::min(a[1], b[1]), std::min(a[2], b[2]));
}

Vector compWiseMax(const Vector& a, const Vector& b) {
	return Vector(std::max(a[0], b[0]), std::max(a[1], b[1]), std::max(a[2], b[2]));
}

Vector compWiseMult(const Vector& a, const Vector& b) {
	return Vector(a[0] * b[0], a[1] * b[1], a[2] * b[2]);
}

double dot (const Vector& a, const Vector& b) {
	return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

double dotPos (const Vector& a, const Vector& b) {
	return std::max(a[0] * b[0] + a[1] * b[1] + a[2] * b[2], 0.);
}

Vector cross(const Vector& a, const Vector& b) {
	return Vector(a[1] * b[2] - a[2] * b[1],
				a[2] * b[0] - a[0] * b[2],
				a[0] * b[1] - a[1] * b[0]);
}

double cosTeta(const Vector& a, const Vector& b) {
	return dot(a, b) / (a.norm() * b.norm());
}

Vector barycenterValue(const Vector& x, const Vector& y, const Vector& z, 
	double u, double v) {
	return x * u + y * v + z * (1 - u - v);
}

Vector rotateX(const Vector& a, double teta) {
	double cost = cos(teta);
	double sint = sin(teta);
	return Vector(a[0], cost*a[1] - sint*a[2], sint*a[1] + cost*a[2]);
}