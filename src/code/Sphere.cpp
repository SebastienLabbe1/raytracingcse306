#include "Sphere.h"
#include "Util.h"
#include <cmath>
#include <iostream>
#define EPS 10e-5

Sphere::Sphere(Vector center, double radius, const MaterialProperties& properties,
Image* texture) : Object(properties, texture), center(center), radius(radius) {
}

bool Sphere::intersect(const Ray& ray, Intersection& intersection) const{
    double radius2 = radius * radius;
    Vector oc = center - ray.origin;
    double t = dot(ray.direction, oc);
    Vector collision = ray.origin + ray.direction * t;
    double distance2 = (collision - center).norm2();
    if(distance2 > radius2) return false;
    double b = std::sqrt(radius2 - distance2);
    if(t > 0) { if(oc.norm2() > radius2 + EPS) b *= -1; }
    else if(oc.norm2() > radius2 - EPS) b *= -1;
    t += b;
    if(t < EPS || t > intersection.t) return false;
    collision += ray.direction * b;
    intersection.update((collision - center) / radius, properties.color, t, this);
    return true;
}

void Sphere::computeAABB() {
    vmin = center - Vector(radius, radius, radius);
    vmax = center + Vector(radius, radius, radius);
}