#include "MaterialProperties.h"

MaterialProperties::MaterialProperties(Vector color, double albedo, 
double opacity, double refractionIndex, bool isLight) : color(color), 
albedo(albedo), opacity(opacity), refractionIndex(refractionIndex), isLight(isLight) {

}