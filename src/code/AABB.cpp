#include "AABB.h"
#include "Util.h"
#include <cmath>
#include <stdexcept>
#include <iostream>
#define EPS 10e-6

AABB::AABB(AABB* right, AABB* left) 
: right(right), left(left) {

}

double AABB::volume2() {
    return (vmax - vmin).norm2();
}

double AABB::volume2With(AABB* aabb) {
    if(!aabb) throw std::runtime_error("volume2with called with nullptr");
    return (compWiseMax(vmax, aabb->vmax) - compWiseMin(vmin, aabb->vmin)).norm2();
}

bool AABB::intersect(const Ray& ray, Intersection& intersection) const {
    if(!left || !right) throw std::runtime_error("AABB intersect called on leaf");
    return intersectAABB(ray, intersection) 
    && ((right && right->intersect(ray, intersection))
    | (left && left->intersect(ray, intersection)));
}

bool AABB::intersectAABB(const Ray& ray, const Intersection& intersection) const {
    //https://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms

    float t1 = (vmin[0] - ray.origin[0])*ray.frac[0];
    float t2 = (vmax[0] - ray.origin[0])*ray.frac[0];
    float t3 = (vmin[1] - ray.origin[1])*ray.frac[1];
    float t4 = (vmax[1] - ray.origin[1])*ray.frac[1];
    float t5 = (vmin[2] - ray.origin[2])*ray.frac[2];
    float t6 = (vmax[2] - ray.origin[2])*ray.frac[2];

    float tmax = std::min(std::min(std::max(t1, t2), std::max(t3, t4)), std::max(t5, t6));
    if(tmax < EPS) return false;
    float tmin = std::max(std::max(std::min(t1, t2), std::min(t3, t4)), std::min(t5, t6));
    return tmax - tmin > EPS && tmin < intersection.t;
}

void AABB::computeAABB() {
    if(right) {
        right->computeAABB();
        if(left) {
            left->computeAABB();
            vmax = compWiseMax(left->vmax, right->vmax);
            vmin = compWiseMin(left->vmin, right->vmin);
        } else {
            vmax = right->vmax;
            vmin = right->vmin;
        }
    } else if(left) {
        left->computeAABB();
        vmax = left->vmax;
        vmin = left->vmin;
    } else {
        vmax.clear();
        vmin.clear();
    }
}

AABB* AABB::insert(AABB* aabb) {
    if(!right) right = aabb;
    else if(!left) left = aabb;
    else {
        computeAABB();
        double extraVolumeRight =  right->volume2With(aabb) - right->volume2();
        double extraVolumeLeft =  left->volume2With(aabb) - left->volume2();

        if(extraVolumeLeft > extraVolumeRight) right = right->insert(aabb);
        else left = left->insert(aabb);
    }
    computeAABB();
    return this;
}

void AABB::printTree() {
    std::cout << "Node " << this << std::endl;
    print(vmax);
    print(vmin);
    std::cout << "right " << std::endl;
    if(right) right->printTree();
    else std::cout << "empty " << std::endl;
    std::cout << "left " << std::endl;
    if(left) left->printTree();
    else std::cout << "empty " << std::endl;
}