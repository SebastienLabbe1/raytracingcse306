#include "Util.h"
#include "Mesh.h"
#include "World.h"
#include "Sphere.h"
#include "Image.h"
#include "Cube.h"

#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <fstream>
#include <vector>

#define RAND (rand() / double(RAND_MAX))
#define RANDE (uniform(engine))

double reflectedProportion(double n1, double n2, double proj) {
    double diff = n1 - n2;
    double summ = n1 + n2;
    double k = diff * diff / (summ * summ);
    double K = 1 - proj;
    return k + (1 - k) * K * K * K * K * K; 
}

double absd(double r) {
    return (r > 0) ? r : -r;
}
 
Vector absv(const Vector& a) {
    return Vector(absd(a[0]), absd(a[1]), absd(a[2]));
}

int binomial(int n, double p) {
    int k = 0;
    for(int i = 0; i < n; i ++) 
        if(RANDE < p) k ++;
    return k;
}

void print(Vector a) {
    std::cout << a[0] << " " << a[1] << " " << a[2] << std::endl;
}

World* loadWorld(std::string filename, bool randomTextures) {
    std::vector<Light> lights;
    std::vector<SLight> slights;
    std::vector<Object*> objects;
    World* world = new World();
    std::ifstream file(std::string("../worlds/") + filename);
    std::string buf;
    Camera* camera = nullptr;
    do {
        file >> buf;
        if(buf == "Camera") {
            Vector origin, scene;
            double height, width, minIntensity, focusDistance, blurStrength, startingIntensity;
            int maxBounces, raysPerPixel;
            file >> origin[0] >> origin[1] >> origin[2];
            file >> scene[0] >> scene[1] >> scene[2];
            file >> height >> width;
            file >> minIntensity >> maxBounces >> raysPerPixel >> startingIntensity;
            file >> focusDistance >> blurStrength;
            camera = new Camera(origin, scene, height, width, minIntensity, 
            focusDistance, blurStrength, startingIntensity, maxBounces, raysPerPixel);
        } else if(buf == "Light") {
            Vector origin, color;
            double intensity;
            file >> origin[0] >> origin[1] >> origin[2];
            file >> color[0] >> color[1] >> color[2];
            file >> intensity;
            if(randomTextures) color = Vector::random();
            //color = Vector::random();
            //origin = Vector::random() - Vector(0, 0.5, 0.5);
            //origin *= 32;
            lights.push_back(Light(origin, color, intensity));
        } else if(buf == "SLight") {
            Vector origin, color;
            double intensity, radius;
            file >> origin[0] >> origin[1] >> origin[2];
            file >> radius;
            file >> color[0] >> color[1] >> color[2];
            file >> intensity;
            if(randomTextures) color = Vector::random();
            color = Vector::random();
            origin = Vector::random() - Vector(0, 0.5, 0.5);
            origin *= 32;
            radius = 1 + RANDE * 3;
            SLight* sl = new SLight(origin, radius, color, intensity);
            slights.push_back(*sl);
            //objects.push_back(&slights.back());
            world->insert(sl);
        } else if(buf == "Sphere") {
            Vector origin, color;
            double radius, albedo, opacity, refractionIndex;
            file >> origin[0] >> origin[1] >> origin[2];
            file >> radius;
            file >> color[0] >> color[1] >> color[2];
            file >> albedo >> opacity >> refractionIndex;
            if(randomTextures) color = Vector::random();
            MaterialProperties properties(color, albedo, opacity, refractionIndex);
            Sphere* sphere = new Sphere(origin, radius, properties);
            world->insert(sphere);
        } else if(buf == "Mesh") {
            file >> buf;
            std::string meshname = std::string("../meshes/") + buf + ".stl";

            file >> buf;
            std::string imagename = std::string("../textures/") + buf + ".ppm";
            Image* image = nullptr;
            if(buf != "None") image = new Image(imagename);
            //if(randomTextures) image = Image::randomSmooth(1000, 1000, 0.1);
            if(randomTextures) image = Image::randomSpread(100, 100, 0.8, 0.8);

            Vector origin, color;
            double albedo, opacity, refractionIndex, size;
            file >> origin[0] >> origin[1] >> origin[2];
            file >> size;
            file >> color[0] >> color[1] >> color[2];
            file >> albedo >> opacity >> refractionIndex;

            MaterialProperties properties(color, albedo, opacity, refractionIndex);
            Mesh* mesh = Mesh::loadStl(meshname, properties, image);
            mesh->move(origin, size);
            world->insert(mesh);
        } else if(buf == "Cube") {
            file >> buf;
            std::string imagename = std::string("../textures/") + buf + ".ppm";
            Image* image = nullptr;
            if(buf != "None") image = new Image(imagename);
            //if(randomTextures) image = Image::randomSmooth(1000, 1000, 0.1);
            if(randomTextures) image = Image::randomSpread(100, 100, 0.8, 0.8);

            Vector origin, diagonal, color;
            double albedo, opacity, refractionIndex;
            file >> origin[0] >> origin[1] >> origin[2];
            file >> diagonal[0] >> diagonal[1] >> diagonal[2];
            file >> color[0] >> color[1] >> color[2];
            file >> albedo >> opacity >> refractionIndex;

            MaterialProperties properties(color, albedo, opacity, refractionIndex);
            Cube* cube = new Cube(origin, diagonal, properties, image);
            world->insert(cube);
        }
    } while(!file.eof());

    world->addLights(lights);
    world->addSLights(slights);
    world->camera = camera;
    return world;
}