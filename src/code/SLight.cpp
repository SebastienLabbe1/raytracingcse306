#include "SLight.h"

SLight::SLight(Vector center, double radius, Vector color, double intensity)
: Sphere(center, radius, MaterialProperties(color * intensity, 0, 0, 0, true)), intensity(intensity) {

}