#ifndef RAY_H
#define RAY_H

#include "Vector.h"

class Ray {
    public:
        Vector origin, direction, frac;

        Ray(Vector origin, Vector direction);
};

#endif // RAY_H