#ifndef OBJECT_H
#define OBJECT_H

#include "Vector.h"
#include "Ray.h"
#include "Intersection.h"
#include "AABB.h"
#include "MaterialProperties.h"
#include "Image.h"


class Object : public AABB {
    public:
        MaterialProperties properties;
        Image* texture;

        Object(const MaterialProperties& properties, Image* texture);

        AABB* insert(AABB* aabb);
};


#endif // OBJECT_H