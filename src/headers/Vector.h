#ifndef VECTOR_H
#define VECTOR_H

#include <random>

static std::default_random_engine engine(10);
static std::uniform_real_distribution<double> uniform(1, 0);


class Vector
{
	private:
		double values[3];

	public:
    	Vector(double x = 0, double y = 0, double z = 0);

		const double& operator[] (int i) const;
		double& operator[] (int i);
		Vector& operator+= (const Vector& vec);
		Vector& operator-= (const Vector& vec);
		Vector& operator*= (double r);
		Vector& operator/= (double r);

		double norm () const;
		double norm2 () const;
		void normalize ();
		Vector normalized() const;
		Vector getOrthogonal() const;

		void copy(const Vector& vec);
		void clear();

		Vector& compWiseMin(const Vector& vec);
		Vector& compWiseMax(const Vector& vec);
		Vector& compWiseMult(const Vector& vec);

		double min() const ;
		double max() const;
		int minI() const;
		int maxI() const;

		Vector randomCos() const;
		Vector randomPow(double alpha, double& costeta) const;

		Vector reflect(const Vector& normal) const;
		Vector refract(const Vector& normal, double n1, double n2) const;

		Vector reflect(const Vector& normal, double proj) const;
		Vector refract(const Vector& normal, double proj, double n1, double n2) const;

		static Vector random(double r);
		static Vector random();
};

Vector operator+ (const Vector& a, const Vector& b);
Vector operator- (const Vector& a, const Vector& b);
Vector operator* (const Vector& a, double r);
Vector operator/ (const Vector& a, double r);

Vector compWiseMin(const Vector& a, const Vector& b);
Vector compWiseMax(const Vector& a, const Vector& b);
Vector compWiseMult(const Vector& a, const Vector& b);

double dot (const Vector& a, const Vector& b);
double dotPos (const Vector& a, const Vector& b);
Vector cross (const Vector& a, const Vector& b);
double cosTeta(const Vector& a, const Vector& b);
Vector barycenterValue(const Vector& x, const Vector& y, const Vector& z, double u, double v);
Vector rotateX(const Vector& a, double teta);

#endif // VECTOR_H
