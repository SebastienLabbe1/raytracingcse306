#ifndef SPHERE_H
#define SPHERE_H

#include "Object.h"
#include "Image.h"

class Sphere : public Object{
    public:
        Vector center;
        double radius;

        Sphere(Vector center, double radius, const MaterialProperties& properties, 
        Image* texture = nullptr);
        
        //AABB methods
        bool intersect(const Ray& ray, Intersection& intersection) const;
        void computeAABB();
};

#endif // SPHERE_H