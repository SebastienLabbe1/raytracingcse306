#ifndef LIGHT_H
#define LIGHT_H

#include "Vector.h"

class Light {
    public:
        Vector source, color;
        double intensity;

        Light(Vector source, Vector color, double intensity);
};

#endif // LIGHT_H