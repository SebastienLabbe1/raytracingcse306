#ifndef WORLD_H
#define WORLD_H

#include "Object.h"
#include "Camera.h"
#include "Light.h"
#include "SLight.h"
#include <vector>
#include <mutex>

struct RayData {
    Vector collision;
    double refractionIndex, nextRefractionIndex, proj;
    int bounces;

    RayData(Vector c, double r, double nr, double p, int b) {
        collision = c;
        refractionIndex = r;
        nextRefractionIndex = nr;
        proj = p;
        bounces = b;
    }
};

class Image;

class World {
    public:
        std::vector<Light> lights;
        std::vector<SLight> slights;
        Camera* camera = nullptr;
        AABB* const root = new AABB();
        MaterialProperties air = MaterialProperties(Vector(), 0., 0., 1.);
        const double rho_d = 1, rho_s = 0.1, alpha = 100, directProportion = 0.5;

        World();

        void insert(AABB* aabb);
        void addLights(std::vector<Light> lights);
        void addSLights(std::vector<SLight> slights);

        void fillImage(Image* const image) const;
        Vector getColor(const Ray& ray, double refractionIndex, int bounces, int rayCount, 
        bool isDiffused) const;
        Vector getColor(const Ray& ray, double refractionIndex, int bounces, 
        bool isDiffused) const;
        bool intersect(const AABB* const aabb, const Ray& ray, Intersection& intersection) const;
};

#endif // WORLD_H