#ifndef INTERSECTION_H
#define INTERSECTION_H

#include "Vector.h"
#include <cfloat>

class Object;

class Intersection {
    public:
        Vector normal, color;
        double t;
        const Object* object = nullptr;

        Intersection(double t = DBL_MAX);

        void update(const Vector& normal, const Vector& color,
        double t, const Object* object);
};

#endif // INTERSECTION_H