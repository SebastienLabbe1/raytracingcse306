#ifndef AABB_H
#define AABB_H

#include "Vector.h"
#include "Ray.h"
#include "Intersection.h"

class AABB {
    public:
        Vector vmin, vmax;
        AABB* right,* left;

        AABB(AABB* right = nullptr, AABB* left = nullptr);

        bool intersectAABB(const Ray& ray, const Intersection& intersection) const;
        double volume2();
        double volume2With(AABB* aabb);

        //Virtual functions for object
        virtual AABB* insert(AABB* aabb);

        //Virtual function for mesh, sphere, cube ...
        virtual bool intersect(const Ray& ray, Intersection& intersection) const;
        virtual void computeAABB();

        //Debug
        void printTree();
};

#endif // AABB_H