#ifndef MESH_H
#define MESH_H

#include "Object.h"

#include <vector>
#include <string>

class Image;

struct Triangle {
	int vtx[3], norm[3], uv[3];

	Triangle(std::vector<int> vtx, std::vector<int> norm, std::vector<int> uv) {
		for(int i = 0; i < 3; i ++) this->vtx[i] = vtx[i];
		for(int i = 0; i < 3; i ++) this->norm[i] = norm[i];
		for(int i = 0; i < 3; i ++) this->uv[i] = uv[i];
	}
};

class Mesh : public Object {
	public:
		std::vector<Vector> vertices, normals, uvs;
		std::vector<Triangle> triangles;

        Mesh(std::vector<Vector> vertices, std::vector<Vector> normals,
		std::vector<Vector> uvs, std::vector<Triangle> triangles, 
		const MaterialProperties& properties, Image* texture = nullptr);

		bool intersectTriangle(const Ray& ray, Intersection& intersection, int iT) const;
		bool intersectTriangle2(const Ray& ray, Intersection& intersection, int iT) const;

		void move(const Vector& translation, double size);
		
		//AABB overloads
		void computeAABB();
		bool intersect(const Ray& ray, Intersection& intersection) const;

		static Mesh* loadStl(std::string filename, 
		const MaterialProperties& properties, Image* texture);
};

#endif // MESH_H
