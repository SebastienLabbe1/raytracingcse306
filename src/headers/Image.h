#ifndef IMAGE_H
#define IMAGE_H

#include <string>

class Vector;

class Image {
    public:
        Vector* buffer = nullptr;
        int height, width, unit;

        Image(std::string filename);
        Image(int height, int width);

        void saveAsPpm(std::string filename, int max_val);
        void loadPpmRaw(std::string filename);
        void loadPpm(std::string filename);
        void spread(int i, int j, double spreadRate, double spreadChance);

        Vector getPixel(const Vector& p) const;
        Vector getPixel(int i, int j) const;
        void setPixel(int i, int j, const Vector& p);
        void setPixel(int i, int j, const Vector& p, double percent);

        static Image* randomSmooth(int height, int width, double changeRate);
        static Image* randomSpread(int height, int width, double spreadRate, double spreadChance);
};

#endif // IMAGE_H