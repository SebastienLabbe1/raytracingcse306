#ifndef SLIGHT_H
#define SLIGHT_H

#include "Sphere.h"
#include "Vector.h"

class SLight : public Sphere {
    public:
        double intensity;

        SLight(Vector center, double radius, Vector color, double intensity);
};




#endif // SLIGHT_H