#ifndef CUBE_H
#define CUBE_H

#include "Object.h"
#include "Image.h"

class Cube : public Object{
    public:
        Vector center, diagonal;

        Cube(Vector center, Vector diagonal, const MaterialProperties& properties,
        Image* texture = nullptr);
        
        //AABB methods
        bool intersect(const Ray& ray, Intersection& intersection) const;
        void computeAABB();
};

#endif // CUBE_H