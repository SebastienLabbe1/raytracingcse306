#ifndef UTIL_H
#define UTIL_H

#include "Vector.h"
#include <string>

class World;
class Mesh;
class Sphere;

double reflectedProportion(double n1, double n2, double proj);

double absd(double r);

Vector absv(const Vector& r);

int binomial(int n, double p);

void print(Vector a);

World* loadWorld(std::string filename, bool randomizeColors = true);

#endif // UTIL_H