#ifndef MATERIAL_PROPERTIES_H
#define MATERIAL_PROPERTIES_H

#include "Vector.h"

class MaterialProperties {
    public:
        Vector color;
        double albedo, opacity, refractionIndex;
        bool isLight;

        MaterialProperties(Vector color, double albedo, 
        double opacity, double refractionIndex, bool isLight = false);
};



#endif // MATERIAL_PROPERTIES_H