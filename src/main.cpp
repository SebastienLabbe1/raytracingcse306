#include "RayTracer.h"

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>
#include <iomanip>


int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "prog <number of images>" << std::endl;
        return 0;
    }
    int imageCount = atoi(argv[1]);
    int height = 400;
    int width = 400;
    int max_val = 1000;

    srand(time(0));


    World* world = loadWorld("world.wd", false);
    Image screen(height, width);

	std::cout << "Creating " << imageCount << " images" << std::endl;

    {
        std::cout << "Start " << std::endl;
        auto begin = std::chrono::high_resolution_clock::now();
        for(int i = 0; i < imageCount; i ++) {
            //std::ostringstream ostr;
            //ostr << std::setfill('0') << std::setw(5) << i;
            world->fillImage(&screen);
            //screen.saveAsPpm(std::string("../video/result") + ostr.str() + ".ppm", max_val);
            //world->camera->rotate(0.01);
        }
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "End " << std::endl;

        std::cout << "Time taken : " << std::chrono::duration_cast<
        std::chrono::nanoseconds>(end-begin).count() / double(10e8) << " s" << std::endl;
    }
    std::stringstream ss;
    ss << time(0);
    std::string ts = ss.str();

    screen.saveAsPpm(std::string("../results/result") + ts + ".ppm", max_val);
    screen.saveAsPpm("../result.ppm", max_val);

    return 0;
}
